import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter number that you sould like to check is it prime: ");
        int n = sc.nextInt();
        System.out.println(isPrime(n));

    }

    public static boolean isPrime (int n) {
        return !new String(new char[n]).matches(".?|(..+?)\\1+");
    }
}
